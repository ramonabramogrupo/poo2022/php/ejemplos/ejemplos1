<!DOCTYPE html>
<!-- 
    Dos div con estas caracteristicas
        con una anchura de 800px : width:800px
        con una altura de 100px : height: 100px
        con tamaño de letra de 40px : font-size: 40px
        con el color de letra blanco : color: white
        con el color de fondo negro : background-color: black

    Aplicando los estilos utilizando un selector por etiqueta
    Los estilos los carga desde una hoja de estilos
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/1.css" />
    </head>
    <body>
        <div>
            Ejemplo de clase
        </div>
        <div>
            Otro texto
        </div>
    </body>
</html>
