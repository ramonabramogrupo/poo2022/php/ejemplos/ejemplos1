<!DOCTYPE html>
<!--
    Colocar una tabla con 3 por 3 celdas
    Las celdas de los 4 extremos las quiero con fondo negro
        background-color:black
    Todas las celdas las quiero con borde rojo
        border:1px solid red;

    Aplicarlo utilizando una hoja de estilos
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/2.css" />
    </head>
    <body>
        <table>
            <tr>
                <td class="esquinas">1</td>
                <td>2</td>
                <td class="esquinas">3</td>
            </tr>
            <tr>
                <td>4</td>
                <td>5</td>
                <td>6</td>
            </tr>
            <tr>
                <td class="esquinas">7</td>
                <td>8</td>
                <td class="esquinas">9</td>
            </tr>
        </table>
    </body>
</html>
