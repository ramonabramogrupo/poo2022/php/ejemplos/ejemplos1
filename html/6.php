<!DOCTYPE html>
<!-- 
    El div este 
        con una anchura de 800px : width:800px
        con una altura de 100px : height: 100px
        con tamaño de letra de 40px : font-size: 40px
        con el color de letra blanco : color: white
        con el color de fondo negro : background-color: black

    Aplicando los estilos en linea
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div style="width:800px;height: 100px;font-size: 40px;color:white;background-color: black;">
            Ejemplo de clase
        </div>
    </body>
</html>
