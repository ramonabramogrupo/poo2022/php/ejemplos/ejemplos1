<!DOCTYPE html>
<!-- 
    Dos div con estas caracteristicas
        con una anchura de 800px : width:800px
        con una altura de 100px : height: 100px
        con tamaño de letra de 40px : font-size: 40px
        con el color de letra blanco : color: white
        con el color de fondo negro : background-color: black

    Aplicando los estilos utilizando un selector por clase
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            .caja{
                width:800px;
                height: 100px;
                font-size: 40px;
                color:white;
                background-color: black;
            }

        </style>
    </head>
    <body>
        <div class="caja">
            Ejemplo de clase
        </div>
        <div class="caja">
            Otro texto
        </div>
    </body>
</html>
