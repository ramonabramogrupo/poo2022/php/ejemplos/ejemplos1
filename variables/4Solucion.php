<style>
    table, tr, td{
        border:1px solid black;
        padding:10px;
    }
</style>

<?php
// colocar las siguientes variables en una tabla colocando
// como encabezado de la tabla los nombres de las variables

$nombre = "Alpe";
$direccion = "Pasaje de peña 1";
$poblacion = "Santander";
?>

<table>
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Direccion</th>
        <th>Poblacion</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= $nombre ?></td>
        <td><?= $direccion ?></td>
        <td><?= $poblacion ?></td>
    </tr>
    </tbody>
</table>