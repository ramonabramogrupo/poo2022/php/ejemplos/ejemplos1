<?php
// utilizar la variable en cada una de las salidas en
// vez de utilizar el texto de forma literal
$texto = "hola";
?>

<?php
echo "<p>hola</p>";
?>


<p>
    <?php
    echo "hola";
    ?>
</p>

<p>
    <?= "hola" ?>
</p>

